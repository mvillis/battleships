package com.training.battleships.domain;

public class Board {
    int [] cells = new int[6];

    public int[] getCells() {
        return cells;
    }

    public void setCells(int[] cells) {
        this.cells = cells;
    }
}
