////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013, Suncorp Metway Limited. All rights reserved.
//
// This is unpublished proprietary source code of Suncorp Metway Limited.
// The copyright notice above does not evidence any actual or intended
// publication of such source code.
//
////////////////////////////////////////////////////////////////////////////////

package com.training.battleships.domain;

import java.util.ArrayList;
import java.util.List;

public final class Player {

    private List<Ship> ships = new ArrayList<Ship>();
    private Board board = new Board();

    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(final List<Ship> ships) {
        this.ships = ships;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(final Board board) {
        this.board = board;
    }
}


