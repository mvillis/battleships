package com.training.battleships.domain;

import java.util.ArrayList;
import java.util.List;

public class Game {
    List<Player> players = new ArrayList<Player>();


    public Game() {
        Player player1 = new Player();
        Player player2 = new Player();
        players.add(player1);
        players.add(player2);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(final List<Player> players) {
        this.players = players;
    }
}
