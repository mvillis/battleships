package com.training.battleships.domain;////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013, Suncorp Metway Limited. All rights reserved.
//
// This is unpublished proprietary source code of Suncorp Metway Limited.
// The copyright notice above does not evidence any actual or intended
// publication of such source code.
//
////////////////////////////////////////////////////////////////////////////////

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public final class MyStepdefs {

    private Game game;

    @Given("^a Battleship Game$")
    public void a_Battleship_Game() throws Throwable {
        this.game = new Game();
        assertThat(this.game, notNullValue());
    }

    @When("^I inspect the contents$")
    public void I_inspect_the_contents() throws Throwable {

    }

    @Then("^I should have one Gameboard$")
    public void I_should_have_one_Gameboard() throws Throwable {
        this.game= new Game();
    }

    @And("^two players$")
    public void two_players() throws Throwable {
        assertThat(this.game.getPlayers().size(), equalTo(2));
    }
}
