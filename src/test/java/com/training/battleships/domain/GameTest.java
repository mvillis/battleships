////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013, Suncorp Metway Limited. All rights reserved.
//
// This is unpublished proprietary source code of Suncorp Metway Limited.
// The copyright notice above does not evidence any actual or intended
// publication of such source code.
//
////////////////////////////////////////////////////////////////////////////////

package com.training.battleships.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;


public final class GameTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSetupGame() throws Exception {
        Game game = new Game();
        assertThat(game, notNullValue());
    }

    @Test
    public void testGameBoardSetup() throws Exception {
        Game game = new Game();
        for (Player player : game.getPlayers()) {
            assertThat(player.getBoard(),notNullValue());
        }
    }

    @Test
    public void testPlayerShipSetup() throws Exception {
        Game game = new Game();

    }


}
