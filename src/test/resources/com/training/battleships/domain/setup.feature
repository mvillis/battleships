Feature: Setup Battleship the Game

  In order to play Battleship
  As a battleship player
  I want to set up the game ready for play

  Background:

    Given a Battleship Game
    And two players

  Scenario: Game Contents

    When I inspect the contents
    Then I should have one Gameboard
#    And one 5 length ship per player
#    And one 4 length ship per player
#    And two 3 length ships per player
#    And one 2 length ship per player
#    And sufficient red hit and white miss markers to record hits and misses for both players for the duration of the game


#  Scenario: Game Setup
#
#    Given a complete Battleship Game Contents
#    When I begin the game
#    Then each player secretly positions their five ships on their lower game board
#    And each ship is positioned either vertically or horizontally
#    And each ship is completely contained on the board
#    And ships on the same board are not overlapping
#    And it is determined who will commence the game
