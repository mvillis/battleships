#Feature: Play Battleship the Game
#
#  In order to demonstrate naval superiority
#  As a battleship player
#  I want to play a game of Battleship with an opponent and win
#
#  Background:
#
#    Given game has been setup
#    And there is not yet a victor
#
#  Scenario: Play - Target Missed
#
#    When the firing player gives his coordinates for a firing solution to the receiving player which does not strike a target
#    Then the receiving player indicates a miss to the firing player
#    And the firing player marks the miss with a white peg in the declared coordinate on their upper grid
#    And the players swap roles
#
#  Scenario: Play - Target Hit but not sunk
#
#    When the firing player gives his coordinates for a firing solution to the receiving player which does strike a target
#    Then the receiving player indicates a hit to the firing player
#    And the firing player marks the hit with a red peg in the declared coordinate on their upper grid
#    And the receiving player marks the hit by placing a red peg in the ship at the declared coordinate on their lower grid
#    And the players swap roles
#
#  Scenario: Play - Target Hit and sunk
#
#    When the firing player gives his coordinates for a firing solution to the receiving player which does strike a target
#    And the target has been struck in all slots
#    Then the receiving player indicates a hit to the firing player
#    And indicates that the ship in question has been sunk
#    And the firing player marks the hit with a red peg in the declared coordinate on their upper grid
#    And the receiving player marks the hit by placing a red peg in the ship at the declared coordinate
#    And the players swap roles
#
#  Scenario: Play - Final Target Hit and Sunk
#
#    When the firing player gives his coordinates for a firing solution to the receiving player which does strike a target
#    And the target has been struck in all slots
#    And the receiving players ships have all been sunk
#    Then the receiving player indicates a hit to the firing player
#    And the firing player marks the hit with a red peg in the declared coordinate on their upper grid
#    And the receiving player marks the hit by placing a red peg in the ship at the declared coordinate
#    And indicates that the final ship in question has been sunk
#    And the firing player is the winner
#
#
